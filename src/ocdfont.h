#ifndef OCDFONT_H
#define OCDFONT_H

#include <allegro5/allegro5.h>
#include <allegro5/allegro_font.h>

ALLEGRO_FONT * ocd_load_font(char * fn);
//void ocdfont_draw_text(ALLEGRO_FONT * fp, char * text, int x, int y

#endif
